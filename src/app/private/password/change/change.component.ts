import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PasswordService } from 'src/app/services/password.service';
import { PasswordChange } from 'src/app/user';

@Component({
  selector: 'app-change',
  templateUrl: './change.component.html',
  styleUrls: ['./change.component.css']
})
export class ChangeComponent implements OnInit {
  changePassword: PasswordChange = {
    email: '',
    oldPassword: '',
    newPassword: ''
  }

  constructor(
    private router: Router,
    private passwordService: PasswordService
  ) { }

  ngOnInit(): void {
  }

  changePswForm = new FormGroup({
    email: new FormControl('', [Validators.required]),
    oldPassword: new FormControl('', [Validators.required]),
    newPassword: new FormControl('', [Validators.required])
  })

  get email(): FormControl {
    return this.changePswForm.get('email') as FormControl;
  }
  get oldPassword(): FormControl {
    return this.changePswForm.get('oldPassword') as FormControl;
  }
  get newPassword(): FormControl {
    return this.changePswForm.get('newPassword') as FormControl;
  }

  onChange(){
    console.log(this.changePswForm.value)
    this.passwordService.updatePsw(this.changePswForm.value)
    .subscribe({
      next: data => {
        console.log(data)
        alert('Password has been updated successfully!')
      },
      error:(e)=>console.error(e) 
    })
  }

}
