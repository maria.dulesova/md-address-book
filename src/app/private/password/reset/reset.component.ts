import { Component, OnInit, Input} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PasswordService } from 'src/app/services/password.service';
import { PasswordReset } from 'src/app/user';


@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit {
  resetPassword: PasswordReset = {
    email:''
  }

  constructor(
    private passwordService: PasswordService
  ) { }

  ngOnInit(): void {
  }

  resetPswForm = new FormGroup({
    email: new FormControl('', [Validators.required])
  })

  get email(): FormControl{
    return this.resetPswForm.get('email') as FormControl;
  }

  onReset(){
    console.log(this.resetPswForm.value)
    this.passwordService.resetPsw(this.resetPswForm.value)
    .subscribe(
      data => {
        console.log(data)
        alert('Password has been reset successfully')
      }
    )
  }

}
