import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Contact } from '../../contact';
import { ContactService } from '../../../services/contact.service';
import { PrefixService } from 'src/app/services/prefix.service';
import { TypeService } from 'src/app/services/type.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  currentContactUpdate: any = [];
  typesUpdate: any = [];
  prefixesUpdate: any = [];

  constructor( 
    public contactService: ContactService,
    public prefixService: PrefixService,
    public typeService: TypeService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getContact(this.route.snapshot.params['id']);
    this.readTypesUpdate();
    this.readPrefixesUpdate();
  }

  updateContactForm: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    lastName: new FormControl('', [Validators.required]),
    telephones: new FormArray([]),
    emails: new FormArray([])
  })
  
  groupControlTelephones = new FormGroup({
    prefix: new FormControl(''),
    number: new FormControl('', [Validators.required]),
    type: new FormControl('')
  });
  
  controlEmail = new FormControl(null, Validators.required);

  get name(): FormControl {
    return this.updateContactForm.get('name') as FormControl;
  }
  get lastName(): FormControl {
    return this.updateContactForm.get('lastName') as FormControl;
  }
  get telephones(): FormArray {
    return this.updateContactForm.get('telephones') as FormArray;
  }
  get emails(): FormArray {
    return this.updateContactForm.get('emails') as FormArray;
  }



  getContact(id: number): void {
    this.contactService.get(id)
      .subscribe(
        contact => {
          this.currentContactUpdate = contact;
          console.log(this.currentContactUpdate);
        }
      )
  }

  readTypesUpdate(){
    this.typeService.getAll()
      .subscribe(
        typeRes => {
          this.typesUpdate = typeRes;
          console.log(this.typesUpdate)
        }
      )
  }

  readPrefixesUpdate(): void{
    this.prefixService.getAll()
      .subscribe(
        prefixRes => {
          this.prefixesUpdate = prefixRes;
          console.log(this.prefixesUpdate)
        }
      )
  }

  onAddPhone() {
    (<FormArray>this.updateContactForm.get('telephones')).push(this.groupControlTelephones);
  }

  onAddEmail() {
    (<FormArray>this.updateContactForm.get('emails')).push(this.controlEmail);
  }

  updateContact(){
    console.log(this.updateContactForm.value);
    this.contactService.update(this.updateContactForm.value)
      .subscribe( data=> {
        alert('Contact Information has been updated Successfully!');
        console.log(data);
      })
  }

}
