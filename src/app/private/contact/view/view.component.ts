import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ContactService } from 'src/app/services/contact.service';
import { Contact } from '../../contact';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  currentContact: any = [];

  constructor(
    private contactService: ContactService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getContact(this.route.snapshot.params['id']);
    this.route.params
    .subscribe({
      next: (params: Params) => {
        this.currentContact.id=params['id']
      },
      error:(e) => console.error(e)
    });
  }

  getContact(id:number): void{
    this.contactService.get(id)
    .subscribe({
      next: contact => {
        this.currentContact = contact; 
        console.log(this.currentContact);
      },
      error: (e) =>console.error(e)
     
    })
  }

  onDelete() {
    this.contactService.delete(this.currentContact.id)
      .subscribe({
        next: response => {
          console.log(response);
        },
        error: (e)=>console.error(e)
      });
    alert('The contact has been deleted successfully!');
    this.router.navigate(['/user/profile/contact/index'], { relativeTo: this.route });
  }

}
