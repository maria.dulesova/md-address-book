import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactService } from 'src/app/services/contact.service';
import { PrefixService } from 'src/app/services/prefix.service';
import { TypeService } from 'src/app/services/type.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  prefixes: any=[];
  types: any = [];
  saved:any = false;
  
  createContactForm: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    lastName: new FormControl('', [Validators.required]),
    telephones: new FormArray([]),
    emails: new FormArray([])
  })

  get name(): FormControl {
    return this.createContactForm.get('name') as FormControl;
  }
  get lastName(): FormControl {
    return this.createContactForm.get('lastName') as FormControl;
  }
  get telephones(): FormArray{
    return this.createContactForm.get('telephones') as FormArray;
  }
  get emails(): FormArray {
    return this.createContactForm.get('emails') as FormArray;
  }
 

  submitted = false;

  constructor(
    public contactService: ContactService,
    private router: Router,
    private route: ActivatedRoute,
    public prefixService: PrefixService,
    public typeService: TypeService
  ) { }

  ngOnInit(): void {
    this.readPrefixes();
    this.readTypes();
  }

  submit() {
    this.contactService.create(this.createContactForm.value)
      .subscribe({
        next: res => {
          console.log(res);
        },
        error: (e)=> console.error(e)
      })
    this.saved = true;
    alert("The contact has been saved successfully!");
    this.createContactForm.reset();
    this.router.navigate(['/user/profile/contact/index'], { relativeTo: this.route });
  }

  readPrefixes(): void{
    this.prefixService.getAll()
    .subscribe({
      next: prefixRes => {
        this.prefixes = prefixRes;
        console.log(this.prefixes);
      },
      error: (e) => console.log(e)
    })
  }
  readTypes(): void {
    this.typeService.getAll()
    .subscribe({
      next: typeRes => {
        this.types = typeRes;
        console.log(this.types);
      },
      error: (e) =>console.error(e)
    })
  }

  onAddPhone(){
    const groupControl = new FormGroup({
      prefix: new FormControl('' ),
      number: new FormControl('', [Validators.required]),
      type: new FormControl('')
    });
    (<FormArray>this.createContactForm.get('telephones')).push(groupControl);
  }

  onAddEmail(){
    const control = new FormControl(null, Validators.required);
    (<FormArray>this.createContactForm.get('emails')).push(control);
  }


}
