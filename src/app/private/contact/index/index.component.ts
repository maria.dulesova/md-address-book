import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactService } from 'src/app/services/contact.service';
import { Contact } from '../../contact';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})

export class IndexComponent implements OnInit {

  contacts: Contact[] = [];

  constructor( 
    public contactService: ContactService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.contactService.getAll()
    .subscribe({
      next: (data: Contact[]) =>{
        this.contacts = data;
        console.log(this.contacts);
      },
      error: (e) =>console.error(e)
    })
  }
  
  
}
