import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { LoggedUser, User } from 'src/app/user';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  isAuthenticated = false;
  userProfile: any = [];
  

  constructor( private userService: UserService) { }

  ngOnInit(): void {
    this.getLogedUser()
    // console.log(this.userProfile)
  }

  getLogedUser(){
    this.userProfile=this.userService.currentUser;
  }

  doLogout(){
    this.userService.logout();
  }

}
