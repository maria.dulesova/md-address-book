import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
  userProfileUpdate: any = this.userService.currentUser;

  constructor(
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    
  }

  updateUserForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    lastName: new FormControl('', [Validators.required, Validators.minLength(3)]),
    email: new FormControl('', [Validators.required, Validators.email])
  })

  get name(): FormControl {
    return this.updateUserForm.get('name') as FormControl;
  }
  get lastName(): FormControl {
    return this.updateUserForm.get('lastName') as FormControl;
  }
  get email(): FormControl {
    return this.updateUserForm.get('email') as FormControl;
  }

  update(){
    console.log(this.updateUserForm.value);
    this.userService.updateLoggedUser(this.updateUserForm.value)
      .subscribe(data => {
        alert('User Information updated successfully!');
        this.router.navigate(['/user/profile'], { relativeTo: this.route });
      })

  }

}
