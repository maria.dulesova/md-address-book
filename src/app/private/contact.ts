export interface Contact {
    "id"?: any;
    "name": string,
    "lastName": string,
    "telephones": Array<Tel>,
    "emails": Array<Email>
}

export interface Tel {
    "prefix": 0,
    "number": string,
    "type": 0

}

export interface Email {
    email: string
}