import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './public/login/login.component';
import { AppRoutingModule } from './app-router.module';
import { SignupComponent } from './public/signup/signup.component';
import { DashboardComponent } from './private/dashboard/dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { UserComponent } from './private/user/view/user.component';
import { IndexComponent } from './private/contact/index/index.component';
import { ViewComponent } from './private/contact/view/view.component';
import { CreateComponent } from './private/contact/create/create.component';
import { EditComponent } from './private/contact/edit/edit.component';
import { UserEditComponent } from './private/user/edit/user-edit/user-edit.component';
import { ChangeComponent } from './private/password/change/change.component';
import { ResetComponent } from './private/password/reset/reset.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    LoginComponent,
    SignupComponent,
    DashboardComponent,
    UserComponent,
    IndexComponent,
    ViewComponent,
    CreateComponent,
    EditComponent,
    UserEditComponent,
    ChangeComponent,
    ResetComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
