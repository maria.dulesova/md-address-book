export interface User {
    name: string;
    lastName: string;
    email: string;
    password: string;
    id?: any;
}

export interface LoggedUser{
    name: string,
    lastName: string,
    email: string;
    id?:any;
}

export interface LoginBody {
    email: string,
    password: string
}

export interface PasswordChange {
    email: string,
    oldPassword: string,
    newPassword: string
}

export interface PasswordReset {
    email: string
}

