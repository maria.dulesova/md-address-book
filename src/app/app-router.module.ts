import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
// import { AuthGuard } from "./guards/auth.guard";

import { HomeComponent } from "./home/home.component";
import { CreateComponent } from "./private/contact/create/create.component";
import { EditComponent } from "./private/contact/edit/edit.component";
import { IndexComponent } from "./private/contact/index/index.component";
import { ViewComponent } from "./private/contact/view/view.component";
import { ChangeComponent } from "./private/password/change/change.component";
import { ResetComponent } from "./private/password/reset/reset.component";
import { UserEditComponent } from "./private/user/edit/user-edit/user-edit.component";
import { UserComponent } from "./private/user/view/user.component";
import { LoginComponent } from "./public/login/login.component";
import { SignupComponent } from "./public/signup/signup.component";


const appRoutes: Routes = [
    { path: '', component: HomeComponent},
    { path: 'signup', component: SignupComponent },
    { path: 'login', component: LoginComponent},
    { path: 'user/profile', component: UserComponent,  children: [
        { path: 'change-password', component: ChangeComponent },
        { path: 'reset-password', component: ResetComponent },
        { path: ':id', component: UserEditComponent},
        { path: 'contact/index', component: IndexComponent},
        { path: 'contact/index/:id', component: ViewComponent },
        { path: 'contact/index/:id/edit', component: EditComponent },
        { path: 'contact/create', component: CreateComponent },        
    ]},
    { path:'**', redirectTo: '', pathMatch: 'full'}
]

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule {

}