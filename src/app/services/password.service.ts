import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { PasswordChange, PasswordReset } from "../user";
import { UserService } from "./user.service";

@Injectable ({
    providedIn: 'root'
})

export class PasswordService{
    constructor(
        private http: HttpClient, 
        private userService: UserService
    ) {}

    updatePsw( changePsw: PasswordChange): Observable<PasswordChange>{
        const headers = new HttpHeaders(
            { 'Authorization': `Bearer ${this.userService.token}` }
        )
        return this.http.post<PasswordChange>(`${environment.apiUrl}authentication/change-password`, changePsw, { headers: headers })
    }

    resetPsw(resetPsw: PasswordReset): Observable<PasswordReset>{
        const headers = new HttpHeaders(
            { 'Authorization': `Bearer ${this.userService.token}` }
        )
        return this.http.post<PasswordReset>(`${environment.apiUrl}authentication/register`, resetPsw, { headers: headers })
    }


}