import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders} from '@angular/common/http';
import { LoggedUser, LoginBody, User } from 'src/app/user';
import { first, Observable} from 'rxjs';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})



export class UserService {
  currentUser: User[] = [];
  token:any = '';

 

  constructor( 
    private http: HttpClient,
    private router: Router) { }

  register(registeredUser: User): Observable<User> {
    return this.http.post<User>(`${environment.apiUrl}/authentication/register`, registeredUser)
  }

  login(login: LoginBody){
    return this.http.post(`${environment.apiUrl}/authentication/login`,login).pipe(first())
      .subscribe(
        (loginResponse:any) => {
          localStorage.setItem( 'auth_token', loginResponse.token);
          this.token = localStorage.getItem('auth_token');
          console.log(loginResponse);
          this.getLoggedUser().subscribe({
            next: (res) => {
              this.currentUser = res;
              console.log(this.currentUser);
            },
            error: (e) =>console.error(e)          
          })
        }
      )
  }

  logout() {
    let removeToken = localStorage.removeItem('auth_token');
    if (removeToken == null) {
      this.router.navigate(['/login']);
    }
  }

  getLoggedUser(): Observable<any>{
    const headers = new HttpHeaders(
      { 'Authorization': `Bearer ${this.token}` }
    )
    return this.http.get(`${environment.apiUrl}user/profile`, { headers: headers })
  }

  updateLoggedUser(userData: LoggedUser): Observable<LoggedUser>{
    const headers = new HttpHeaders(
      { 'Authorization': `Bearer ${this.token}` }
    )
    return this.http.put<LoggedUser>(`${environment.apiUrl}user`, userData, { headers: headers })
     
  }

}


