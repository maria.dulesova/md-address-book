import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { UserService } from "./user.service";

@Injectable({
    providedIn: 'root'
})
export class TypeService {

    constructor(private http: HttpClient, private userService: UserService) { }

    getAll(): Observable<any> {
        const headers = new HttpHeaders(
            { 'Authorization': `Bearer ${this.userService.token}` }
        )
        return this.http.get(`${environment.apiUrl}/telephone-type`, { headers: headers })

        
    }


}