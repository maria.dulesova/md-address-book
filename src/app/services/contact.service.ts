import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';

import { Observable } from 'rxjs';
import { Contact } from '../private/contact';
import { environment } from 'src/environments/environment';
import { UserService } from 'src/app/services/user.service';


@Injectable({
  providedIn: 'root'
})

export class ContactService {

  constructor(private http: HttpClient, private userService: UserService) { }

  create(contact: Contact): Observable<Contact>{
    const headers = new HttpHeaders(
      { 'Authorization': `Bearer ${this.userService.token}` }
    )
    return this.http.post<Contact>(`${environment.apiUrl}contact`, contact, { headers: headers })
  }

  getAll():Observable < any > {
      const headers = new HttpHeaders(
        { 'Authorization': `Bearer ${this.userService.token}` }
      )
        return this.http.get(`${environment.apiUrl}contact`, { headers: headers })
  }

  get(id: number): Observable<any> {
    const headers = new HttpHeaders(
      { 'Authorization': `Bearer ${this.userService.token}` }
    )
    return this.http.get(`${environment.apiUrl}contact/${id}`, { headers: headers });
  }

  update(contactData: Contact): Observable <Contact>{
    const headers = new HttpHeaders(
      { 'Authorization': `Bearer ${this.userService.token}` }
    )
    return this.http.put<Contact>(`${environment.apiUrl}contact`, contactData, { headers: headers });
  }

  delete(id:number): Observable<any> {
    const headers = new HttpHeaders(
      { 'Authorization': `Bearer ${this.userService.token}` }
    )
    return this.http.delete(`${environment.apiUrl}contact/${id}`, { headers: headers });
  }

}
