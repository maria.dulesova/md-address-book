import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { User } from 'src/app/user';
import { HttpClient } from '@angular/common/http';
import { first } from 'rxjs';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  user: User[]=[];
  registrationSucceded = false;

  constructor(
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient) { }
  
  signupForm: FormGroup = new FormGroup({
    name: new FormControl(null, [Validators.required, Validators.minLength(3)]),
    lastName: new FormControl(null, [Validators.required, Validators.minLength(3)]),
    email: new FormControl(null, [Validators.required, Validators.email]),
    password: new FormControl(null, [Validators.required, Validators.minLength(8)])
  })

  get name(): FormControl {
    return this.signupForm.get('name') as FormControl;
  }
  get lastName(): FormControl {
    return this.signupForm.get('lastName') as FormControl;
  }
  get email(): FormControl {
    return this.signupForm.get('email') as FormControl;
  }
  get password(): FormControl {
    return this.signupForm.get('password') as FormControl;
  }

  ngOnInit():void { 
  }
  onRegister(){
    if (this.signupForm.invalid) {
      return;
    }
    this.userService.register(this.signupForm.value).pipe(first())
      .subscribe({
        next: () => {
          this.onLoginClick();
        },
        error: error => {
         console.log(error);
        }
      });
  }

  //Routes
  onLoginClick(){
    this.router.navigate(['/login'], { relativeTo: this.route });
  }
}


