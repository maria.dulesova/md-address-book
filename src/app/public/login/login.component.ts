import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {  LoginBody, User } from 'src/app/user';
import { UserService } from '../../services/user.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  registeredUsers: User[] = [];
  token = [];
  loginSucceded = false;


  constructor( 
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService) { }

  ngOnInit() {
  }

  onSignupClick(){
    this.router.navigate(['/signup'])
  }

  //Forms
  loginForm = new FormGroup({
    email: new FormControl(null, [Validators.required]),
    password: new FormControl(null, [Validators.required])

  })
  get email(): FormControl {
    return this.loginForm.get('email') as FormControl
  }
  get password(): FormControl {
    return this.loginForm.get('password') as FormControl
  }

  onLogin(){
    let login: LoginBody = {
      email : this.loginForm.value.email,
      password: this.loginForm.value.password
    }

    if(this.loginForm.invalid){
      return;
    }
    this.userService.login(login)
    const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/user/profile';
    this.router.navigateByUrl(returnUrl);
  }

}

